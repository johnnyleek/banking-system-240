package us.JohnnyLeek.BankingSystem;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Accounts.Account;
import us.JohnnyLeek.BankingSystem.Accounts.AccountType;
import us.JohnnyLeek.BankingSystem.Controllers.AccountListController;
import us.JohnnyLeek.BankingSystem.Controllers.AccountOperationController;
import us.JohnnyLeek.BankingSystem.Controllers.TransactionController;
import us.JohnnyLeek.BankingSystem.Controllers.UserController;
import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.DAOs.AddressDAO;
import us.JohnnyLeek.BankingSystem.DAOs.TransactionDAO;
import us.JohnnyLeek.BankingSystem.DAOs.UserDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountAgeException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountCloseException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.TransactionNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserAlreadyExistsException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Users.Address;
import us.JohnnyLeek.BankingSystem.Users.AddressState;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class tests the banking system thoroughly. Run this class to test the entire functionality of the system.
 * @author leek1j
 *
 */
public class BankingSystem {

	public static void main(String[] args) {
		try {
			
			System.out.println("======================[TEST]======================");
			System.out.println("Initialize DAOS...");
			AccountDAO accountDAO = AccountDAO.getInstance();
			AddressDAO addressDAO = AddressDAO.getInstance();
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			UserDAO userDAO = UserDAO.getInstance();
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("======================[TEST]======================");
			System.out.println("Initializing Controllers...");
			UserController userController = new UserController(userDAO);
			AccountListController accountListController = new AccountListController(accountDAO);
			AccountOperationController accountOperationController = new AccountOperationController();
			TransactionController transactionController = new TransactionController(transactionDAO);
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("======================[TEST]======================");
			System.out.println("Testing User Controller...");
			
			System.out.println("Create User... (Should pass)");
			userController.addUser("Johnny2", "", "Leek", LocalDate.of(2001, 8, 8), "987654321", "Programmer", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.ALABAMA, 49316);
			
			System.out.println("Create User with incorrect num of characters license number... (Should fail)");
			try {
				userController.addUser("Johnny2", "", "Leek", LocalDate.of(2001, 8, 8), "98765432", "Programmer", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.ALABAMA, 49316);
			} catch(IllegalArgumentException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Create User with non-numeric license number... (Should fail)");
			try {
				userController.addUser("Johnny2", "", "Leek", LocalDate.of(2001, 8, 8), "98765a321", "Programmer", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.ALABAMA, 49316);
			} catch(IllegalArgumentException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Create User with duplicate license number... (Should fail)");
			try {
				userController.addUser("Johnny2", "", "Leek", LocalDate.of(2001, 8, 8), "987654321", "Programmer", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.ALABAMA, 49316);
			} catch(UserAlreadyExistsException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Get User by license number \"987654321\"... (Should pass)");
			try {
				userController.getUser("987654321");
			} catch (UserNotFoundException e) {
				System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
				e.printStackTrace();
			}
			
			System.out.println("Remove User by license number \"987654321\"... (Should pass)");
			try {
				userController.removeUser("987654321");
			} catch (UserNotFoundException e) {
				System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
				e.printStackTrace();
			}
			
			System.out.println("Re-adding that removed user... (Should pass)");
			userController.addUser("Johnny2", "", "Leek", LocalDate.of(2001, 8, 8), "987654321", "Programmer", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.ALABAMA, 49316);
			
			System.out.println("Updating User with new User Object... (Should pass)");
			try {
				Address replaceAddress = new Address("987654321", 1234, 0, "Test road", "Test city", AddressState.CALIFORNIA, 90210);
				User replaceUser = new User("Updated", "New", "User", LocalDate.now(), "987654321", "None", replaceAddress);
				userController.updateUser(replaceUser);
			} catch(UserNotFoundException e) {
				System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
				e.printStackTrace();
			}
			
			System.out.println("Recreating original user to continue tests... (Should pass)");
			try {
				Address replaceAddress2 = new Address("987654321", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.MICHIGAN, 49316);
				User replaceUser2 = new User("Johnny", "", "Leek", LocalDate.of(2001, 8, 8), "987654321", "Prorammer", replaceAddress2);
				userController.updateUser(replaceUser2);
			} catch(UserNotFoundException e) {
				System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
				e.printStackTrace();
			}
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("======================[TEST]======================");
			System.out.println("Testing Account List Controller...");
			
			
			System.out.println("Creating a Business Checking Account... (Should pass)");
			accountListController.addAccount(AccountType.BUSINESS_CHECKING, userController.getUser("987654321"));
			
			System.out.println("Creating another Business Checking Account... (Should fail)");
			try {
				accountListController.addAccount(AccountType.BUSINESS_CHECKING, userController.getUser("987654321"));
			} catch(UserExistingAccountException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Creating a Student Checking Account... (Should pass)");
			accountListController.addAccount(AccountType.STUDENT_CHECKING, userController.getUser("987654321"));
			
			System.out.println("Creating a 13 year old user... (Should pass)");
			userController.addUser("Young", "", "User", LocalDate.of(2007, 1, 1), "123456789", "Student", 8420, 0, "Curley Trail SE", "Grand Rapids", AddressState.MICHIGAN, 49316);
			
			System.out.println("Trying to create a Student Checking Account for 13 y/o... (Should fail)");
			try {
				accountListController.addAccount(AccountType.STUDENT_CHECKING, userController.getUser("123456789"));
			} catch(AccountAgeException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Trying to create a Student Checking Account for 13 y/o with adult... (Should pass)");
			try {
				List<User> adultAndChild = new ArrayList<User>();
				adultAndChild.add(userController.getUser("123456789"));
				adultAndChild.add(userController.getUser("987654321"));
				accountListController.addAccount(AccountType.STUDENT_CHECKING, adultAndChild);
			} catch(AccountAgeException | UserExistingAccountException e) {
				System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
				e.printStackTrace();
			}
			
			System.out.println("Trying to close \"Johnny\"'s First account... (Should pass)");
			accountListController.closeAccount(accountDAO.getAccounts("987654321").get(0).getAccountNumber(), userController.getUser("987654321"));
			
			System.out.println("Opening that account again... (Should pass)");
			accountListController.openAccount(accountDAO.getAccounts("987654321").get(0).getAccountNumber());
			
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("======================[TEST]======================");
			System.out.println("Testing Account Operation Controller...");
			
			System.out.println("Depositing $500 into all of \"Johnny\"'s accounts... (Should pass)");
			for(Account account : accountListController.getAccounts("987654321")) {
				try {
					accountOperationController.deposit(account, "500.00", userController.getUser("987654321"));
				} catch(AmountInvalidException e) {
					System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
					e.printStackTrace();
				}
			}
			
			System.out.println("Depositing an invalid amount (abc123) into \"Johnny\"'s first account... (Should fail)");
			try {
				accountOperationController.deposit(accountListController.getAccounts("987654321").get(0), "abc123", userController.getUser("987654321"));
			} catch(AmountInvalidException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Withdrawing $5 from all of \"Johnny\"'s accounts... (Should pass)");
			for(Account account : accountListController.getAccounts("987654321")) {
				try {
					System.out.println("Old Balance of " + account.getAccountNumber() + ": " + accountOperationController.getAccountBalance(account));
					accountOperationController.withdraw(account, "5.00", userController.getUser("987654321"));
					System.out.println("New Balance of " + account.getAccountNumber() + ": " + accountOperationController.getAccountBalance(account));
				} catch(AmountInvalidException e) {
					System.out.println("Test failed when it shouldn't have. Terminating program. Stack trace:");
					e.printStackTrace();
				}
			}
			
			System.out.println("Withdrawing $5000 from \"Johnny\"'s first account... (Should fail)");
			try {
				accountOperationController.withdraw(accountListController.getAccounts("987654321").get(0), "5000.00", userController.getUser("987654321"));
			} catch(AmountInvalidException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Closing \"Johnny\"'s first account, to test next step... (Should pass)");
			accountListController.closeAccount(accountDAO.getAccounts("987654321").get(0).getAccountNumber(), userController.getUser("987654321"));
			
			System.out.println("Attempting to withdraw from closed account... (Should fail)");
			try {
				accountOperationController.withdraw(accountListController.getAccounts("987654321").get(0), "5.00", userController.getUser("987654321"));
			} catch(AccountInactiveException e) {
				System.out.println("Test Failed: " + e.getMessage());
				
			}			
			
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("======================[TEST]======================");
			System.out.println("Testing Transaction Controller...");
			System.out.println("Getting transaction with invalid number... (Should fail)");
			try {
				transactionController.getTransaction(new BigInteger("123"));
			} catch(TransactionNotFoundException e) {
				System.out.println("Test Failed: " + e.getMessage());
			}
			
			System.out.println("Printing all transactions today... (Should pass)");
			List<Transaction> allTransactionsToday = transactionController.getAllTransactionsDate(LocalDate.now());
			for(Transaction transaction : allTransactionsToday) {
				System.out.println(transaction);
			}
			
			System.out.println("Printing all transactions within range... (Should pass)");
			List<Transaction> allTransactionsRange = transactionController.getAllTransactionsRange(LocalDate.now().minusDays(1), LocalDate.now());
			for(Transaction transaction : allTransactionsRange) {
				System.out.println(transaction);
			}
			
			System.out.println("Printing all transactions for \"Johnny\"'s first account... (Should pass)");
			List<Transaction> allTransactionsAccount = transactionController.getAllTransactionsAccount(accountListController.getAccounts("987654321").get(0).getAccountNumber());
			for(Transaction transaction : allTransactionsAccount) {
				System.out.println(transaction);
			}
			
			System.out.println("Printing all transactions for \"Johnny\"'s second account today only... (Should pass)");
			List<Transaction> allTransactionsAccountDate = transactionController.getAllTransactionsAccountDate(accountListController.getAccounts("987654321").get(1).getAccountNumber(), LocalDate.now());
			for(Transaction transaction : allTransactionsAccountDate) {
				System.out.println(transaction);
			}
			
			System.out.println("Printing all transactions for \"Johnny\"'s first account within range... (Should pass)");
			List<Transaction> allTransactionsAccountRange = transactionController.getAllTransactionsAccountRange(accountListController.getAccounts("987654321").get(0).getAccountNumber(), LocalDate.now().minusDays(1), LocalDate.now());
			for(Transaction transaction : allTransactionsAccountRange) {
				System.out.println(transaction);
			}
			System.out.println("======================[TEST]======================\n\n\n");
			
			System.out.println("==================================================");
			System.out.println("|                                                |");
			System.out.println("|                 TESTS COMPLETE                 |");
			System.out.println("|          Every test ran successfully!          |");
			System.out.println("|                                                |");
			System.out.println("==================================================");
			
		} catch (DAOReadException | DAOWriteException | IllegalArgumentException | UserAlreadyExistsException | AccountAgeException | UserExistingAccountException | UserNotFoundException | AccountNotFoundException | AccountInactiveException | AmountInvalidException | AccountCloseException e) {
			System.out.println("Something went wrong (outside of testing scope). Try deleting all files in the \"data\" directory, and running the program again.");
			e.printStackTrace();
		}
		

	}
	
}
