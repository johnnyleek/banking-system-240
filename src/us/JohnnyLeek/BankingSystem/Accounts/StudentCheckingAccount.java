package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.util.List;

import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountAgeException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Transactions.TransactionType;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * Implementation of a Student Checking Account
 * Requirements:
 * 	- Must be between ages of 17-23 or above 12 with an additional user above 18
 * 	- No minimum balance
 * 	- $35 overdraft fee. Maximum overdraft of $500
 *  - No monthly fee
 * @author leek1j
 *
 */
public class StudentCheckingAccount extends CheckingAccount {

	/**
	 * Constructor for opening with multiple users
	 * @param accountOwners - List of authorized users
	 * @throws AccountAgeException if list of users does not meet age requirements to open account
	 * @throws UserExistingAccountException if one or more users already owns an account of this type
	 */
	public StudentCheckingAccount(List<User> accountOwners) throws AccountAgeException, UserExistingAccountException {
		super(accountOwners, new BigDecimal("35"), new BigDecimal("-500"), false, BigDecimal.ZERO);
		
		if(!super.meetsAgeRequirements(accountOwners))
			return;
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			for(User user : accountOwners) {
				List<Account> accounts;
					accounts = accountDAO.getAccounts(user.getLicenseNumber());
					boolean doesntOwn = false;
					for(Account account : accounts) {
						if(super.getUserAge(user) >= 18) {
							for(User otherUser : accountOwners) {
								if(super.getUserAge(otherUser) < 17) {
									doesntOwn = true;
									break;
								}
							}
						}
						if(account instanceof StudentCheckingAccount && !doesntOwn)
							throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
					}
			}
		} catch (DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor for opening with one user
	 * @param user - User to open account
	 * @throws AccountAgeException if user does not meet age requirements 
	 * @throws UserExistingAccountException if user already owns an account of this type
	 */
	public StudentCheckingAccount(User user) throws AccountAgeException, UserExistingAccountException {
		super(user, new BigDecimal("35"), new BigDecimal("-500"), false, BigDecimal.ZERO);

		List<Account> accounts;
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			accounts = accountDAO.getAccounts(user.getLicenseNumber());
			for(Account account : accounts) {
				if(account instanceof StudentCheckingAccount)
					throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
			}
		} catch (DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
		}
		
		int ageInYears = super.getUserAge(user);
		
		if( !(ageInYears >= 17 && ageInYears <= 23) ) {
			throw new AccountAgeException("User cannot open account alone or User is too old. Must be between 17 and 23 years of age (User is " + ageInYears + ")");
		}
		
	}

	/**
	 * @see #deposit(String)
	 */
	@Override
	public Transaction deposit(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validDepositAmt = super.getValidAmount(amount);
		BigDecimal depositResult = super.getBalance().add(validDepositAmt);
		
		//Set the balance
		super.setBalance(depositResult);
		
		//If the users account is overdrawn, and this transaction brings their account into the positive range, set it back to active
		if(super.getAccountStatus() == AccountStatus.OVERDRAWN) {
			if(depositResult.compareTo(BigDecimal.ZERO) >= 0) {
				super.setAccountStatus(AccountStatus.ACTIVE);
				System.out.println("Account " + super.getAccountNumber() + " is no longer overdrawn.");
			}
		}
		return new Transaction(validDepositAmt, depositResult, TransactionType.DEPOSIT, super.getAccountNumber(), user.getLicenseNumber());
	}

	/**
	 * @see #withdraw(String)
	 */
	@Override
	public Transaction withdraw(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validWithdrawAmt = super.getValidAmount(amount);
		BigDecimal withdrawResult = super.getBalance().subtract(validWithdrawAmt);
		
		//If the user hasn't opted into overdrafting, and this withdraw would bring balance below 0, reject transaction
		if(withdrawResult.compareTo(BigDecimal.ZERO) < 0 && !super.isCanOverdraft()) {
			throw new AmountInvalidException("Account " + super.getAccountNumber() + " has not opted in for overdrafting.");
		}
		if(withdrawResult.compareTo(super.getMaximumOverDraft()) >= 0) {
			//If this transaction would bring the user below $0, set status to "Overdrawn"
			if(withdrawResult.compareTo(BigDecimal.ZERO) < 0) {
				withdrawResult = withdrawResult.subtract(super.getOverDraftFee());
				if(super.getAccountStatus() != AccountStatus.OVERDRAWN) {
					super.setAccountStatus(AccountStatus.OVERDRAWN);
					System.out.println("Account " + super.getAccountNumber() + " is now overdrawn.");
				}
			}
			
			//Set the balance
			super.setBalance(withdrawResult);
			return new Transaction(validWithdrawAmt, withdrawResult, TransactionType.WITHDRAW, super.getAccountNumber(), user.getLicenseNumber());
		}
		
		throw new AmountInvalidException("Amount would exceed overdraft limit.");
	}
	
	/**
	 * Requirements:
	 *   No monthly-fee for the account
	 * @see #calculateMonthlyFee()
	 */
	@Override
	public BigDecimal calculateMonthlyFee() {
		return super.getMonthlyFee();
	}
	
	/**
	 * Prints the account in a English readable format
	 */
	@Override
	public String toString() {
		return "Student Checking Account: \n" +
			   "Account Number: " + super.getAccountNumber() + "\n" +
			   "Balance: " + super.getBalance() + "\n" +
			   "Account Owner: " + super.getAccountOwners() + "\n" +
			   "Monthly Fee: " + super.getMonthlyFee() + "\n" +
			   "Can Overdraft: " + super.isCanOverdraft() + "\n" +
			   "Status: " + super.getAccountStatus() + "\n\n";
	}
	
	
}
