package us.JohnnyLeek.BankingSystem.Accounts;
/**
 * Contains all account status's
 * @author leek1j
 *
 */
public enum AccountStatus {
	ACTIVE,
	OVERDRAWN,
	CLOSED,
	FROZEN,
	LOCKED
}
