package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.DAOs.TransactionDAO;
import us.JohnnyLeek.BankingSystem.DAOs.UserDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Transactions.TransactionType;
import us.JohnnyLeek.BankingSystem.Users.User;
/**
 * Implementation of a Personal Checking Account
 * Requirements:
 * 	- No opening restrictions
 * 	- No minimum balance
 * 	- $35 overdraft fee. Maximum overdraft of $1500
 *  - Monthly fee of $10 unless deposit of $500 or more a month or combined average of $5000 over all linked accounts
 *  - If number of transactions exceeds 100 per month, each subsequent transaction will deduct $0.25 from the account
 * @author leek1j
 *
 */
public class PersonalCheckingAccount extends CheckingAccount {
	
	/**
	 * Constructor for opening with multiple users
	 * @param accountOwners - List of authorized users
	 * @throws UserExistingAccountException if one or more users already owns an account of this type
	 */
	public PersonalCheckingAccount(List<User> accountOwners) throws UserExistingAccountException {
		super(accountOwners, new BigDecimal("35"), new BigDecimal("-1500"), false, new BigDecimal("10"));
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			for(User user : accountOwners) {
				List<Account> accounts;
				accounts = accountDAO.getAccounts(user.getLicenseNumber());
				
				for(Account account : accounts) {
					if(account instanceof PersonalCheckingAccount)
						throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
				}
				
			}
		} catch(DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * Constructor for opening with one user
	 * @param user - User to own account
	 * @throws UserExistingAccountException if user already owns an account of this type
	 */
	public PersonalCheckingAccount(User user) throws UserExistingAccountException{
		super(user, new BigDecimal("35"), new BigDecimal("-1500"), false, new BigDecimal("10"));
		List<Account> accounts;
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			accounts = accountDAO.getAccounts(user.getLicenseNumber());
			for(Account account : accounts) {
				if(account instanceof PersonalCheckingAccount)
					throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
			}
		} catch (DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
		}
	}
	
	/**
	 * @see #deposit(String)
	 */
	@Override
	public Transaction deposit(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validDepositAmt = super.getValidAmount(amount);
		BigDecimal depositResult = super.getBalance().add(validDepositAmt);
		
		//Set the balance
		super.setBalance(depositResult);
		
		//If the users account is overdrawn, and this transaction brings their account into the positive range, set it back to active
		if(super.getAccountStatus() == AccountStatus.OVERDRAWN) {
			if(depositResult.compareTo(BigDecimal.ZERO) >= 0) {
				super.setAccountStatus(AccountStatus.ACTIVE);
				System.out.println("Account " + super.getAccountNumber() + " is no longer overdrawn.");
			}
		}
		
		return new Transaction(validDepositAmt, depositResult, TransactionType.DEPOSIT, super.getAccountNumber(), user.getLicenseNumber());
		
	}
	
	/**
	 * @see #withdraw(String)
	 */
	@Override
	public Transaction withdraw(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validWithdrawAmt = super.getValidAmount(amount);
		BigDecimal withdrawResult = super.getBalance().subtract(validWithdrawAmt);
		
		//If the user hasn't opted into overdrafting, and this withdraw would bring balance below 0, reject transaction
		if (withdrawResult.compareTo(BigDecimal.ZERO) < 0 && !super.isCanOverdraft()) {
			throw new AmountInvalidException("Account " + super.getAccountNumber() + " has not opted in for overdrafting.");
		}
		if (withdrawResult.compareTo(super.getMaximumOverDraft()) >= 0) {
			//If this transaction would bring the user below $0, set the status to "Overdrawn"
			if(withdrawResult.compareTo(BigDecimal.ZERO) < 0) {
				withdrawResult = withdrawResult.subtract(super.getOverDraftFee());
				if(super.getAccountStatus() != AccountStatus.OVERDRAWN) {
					super.setAccountStatus(AccountStatus.OVERDRAWN);
					System.out.println("Account " + super.getAccountNumber() + " is now overdrawn.");
				}
			}
			//Set the balance
			super.setBalance(withdrawResult);
			return new Transaction(validWithdrawAmt, withdrawResult, TransactionType.WITHDRAW, super.getAccountNumber(), user.getLicenseNumber());
		}
		throw new AmountInvalidException("Amount would exceed overdraft limit.");
	}
	
	/**
	 * Requirements:
	 *  A monthly fee of $10 shall be assessed from the account unless the user
	 *	makes a deposit of $500 more a month or there is a combined average of
	 *	$5000 among all linked checking and savings accounts.
	 * @see #calculateMonthlyFee()
	 */
	@Override
	public BigDecimal calculateMonthlyFee() {
		try {
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			AccountDAO accountDAO = AccountDAO.getInstance();
			
			LocalDate monthStart = LocalDate.now().withDayOfMonth(1).minusMonths(1);
			LocalDate monthEnd = LocalDate.now().withDayOfMonth(1).minusDays(1);	
			
			List<Transaction> monthlyTransactions = transactionDAO.getAllTransactionsAccountRange(super.getAccountNumber(), monthStart, monthEnd);
			for(Transaction transaction : monthlyTransactions) {
				if(transaction.getTransactionType() == TransactionType.DEPOSIT && transaction.getAmount().compareTo(new BigDecimal("500")) >= 0) {
					return BigDecimal.ZERO;
				}
			}
			
			List<Account> linkedAccounts = new ArrayList<Account>();
			for(User user : super.getAccountOwners()) {
				
				try {
					List<Account> userAccounts = accountDAO.getAccounts(user.getLicenseNumber());
					
					for(Account account : userAccounts) {
						if(!linkedAccounts.contains(account))
							linkedAccounts.add(account);
						else
							continue;
					}
					
				} catch (UserNotFoundException e) {
					continue;
				}
			}
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			for(Account account : linkedAccounts) {
				
				List<Transaction> monthlyAccountTransactions = transactionDAO.getAllTransactionsAccountRange(account.getAccountNumber(), monthStart, monthEnd);
				for(Transaction transaction : monthlyAccountTransactions) {
					if(transaction.getTransactionType() == TransactionType.DEPOSIT)
						totalAmount = totalAmount.add(transaction.getAmount());
				}
				
			}
			
			if(totalAmount.compareTo(new BigDecimal("5000")) >= 0)
				return BigDecimal.ZERO;
			
		} catch(DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong...");
			e.printStackTrace();
		}
		
		return super.getMonthlyFee();
	}
	
	/**
	 * Prints the account in a English readable format
	 */
	@Override
	public String toString() {
		return "Personal Checking Account: \n" +
			   "Account Number: " + super.getAccountNumber() + "\n" +
			   "Balance: " + super.getBalance() + "\n" +
			   "Account Owner: " + super.getAccountOwners() + "\n" +
			   "Monthly Fee: " + super.getMonthlyFee() + "\n" +
			   "Can Overdraft: " + super.isCanOverdraft() + "\n" +
			   "Status: " + super.getAccountStatus() + "\n\n";
	}
	
}
