package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class serves as a template for a CheckingAccount. Extends from @see {@link Account}
 * Global Fields:
 * 		- overDraftFee: OverDraft Fee for the account
 * 		- maximumOverDraft: The maximum overdraft balance for the account
 * 		- canOverDraft: If the user has opted into overdrafting or not
 * @author leek1j
 *
 */
public abstract class CheckingAccount extends Account {
	
	private BigDecimal overDraftFee;
	private BigDecimal maximumOverDraft;
	
	private boolean canOverdraft;
	
	/**
	 * Constructor for multiple authorized users
	 * @param accountOwners - list of users that own account
	 * @param overDraftFee - overdraft fee for account
	 * @param maximumOverDraft - maximum overdraft for the account
	 * @param canOverdraft - can the user overdraft
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public CheckingAccount(List<User> accountOwners, BigDecimal overDraftFee, BigDecimal maximumOverDraft, boolean canOverdraft, BigDecimal monthlyFee) {
		super(accountOwners, monthlyFee);
		this.overDraftFee = overDraftFee;
		this.maximumOverDraft = maximumOverDraft;
		this.canOverdraft = canOverdraft;
	}
	
	/**
	 * Constructor for only one authorized user
	 * @param user - owner of account
	 * @param overDraftFee - overdraft fee for the account
	 * @param maximumOverDraft - maximum overdraft for the account
	 * @param canOverdraft - can the user overdraft
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public CheckingAccount(User user, BigDecimal overDraftFee, BigDecimal maximumOverDraft, boolean canOverdraft, BigDecimal monthlyFee) {
		super(user, monthlyFee);
		this.overDraftFee = overDraftFee;
		this.maximumOverDraft = maximumOverDraft;
		this.canOverdraft = canOverdraft;
	}

	/* GETTERS AND SETTERS */
	public BigDecimal getOverDraftFee() {
		return overDraftFee;
	}

	public void setOverDraftFee(BigDecimal overDraftFee) {
		this.overDraftFee = overDraftFee;
	}

	public BigDecimal getMaximumOverDraft() {
		return maximumOverDraft;
	}

	public void setMaximumOverDraft(BigDecimal maximumOverDraft) {
		this.maximumOverDraft = maximumOverDraft;
	}

	public boolean isCanOverdraft() {
		return canOverdraft;
	}

	public void setCanOverdraft(boolean canOverdraft) {
		this.canOverdraft = canOverdraft;
	}
	
	
	
}
