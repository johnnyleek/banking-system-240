package us.JohnnyLeek.BankingSystem.Accounts;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Exceptions.AccountAgeException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Users.User;
/**
 * This class serves as the template for every account, holds fields shared by every account
 * Global Fields:
 * 		- accountNumber: Account number for the account. 9 digit randomly generated BigInteger
 * 		- balance: Accounts current balance
 * 		- accountOwners: List of current Users who are authorized to use the account
 * 		- monthlyFee: Monthly Fee charged by the account to operate
 * 		- status: The current status of the account [ACTIVE, OVERDRAWN, CLOSED, FROZEN, or LOCKED]
 * @author leek1j
 *
 */
public abstract class Account implements Serializable {

	private BigInteger accountNumber;
	private BigDecimal balance;
	private List<User> accountOwners = new ArrayList<User>();
	private BigDecimal monthlyFee;
	private AccountStatus status;

	/**
	 * Constructor for multiple authorized users
	 * @param accountOwners - list of users that own account
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public Account(List<User> accountOwners, BigDecimal monthlyFee) {
		accountNumber = new BigInteger(generateAccountNumber());
		this.balance = BigDecimal.ZERO;
		this.monthlyFee = monthlyFee;
		this.status = AccountStatus.ACTIVE;
		
		//Add all users to account owners
		for(User user : accountOwners) {
			this.accountOwners.add(user);			
		}
	}
	
	/**
	 * Constructor for only one authorized user
	 * @param user - owner of account
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public Account(User user, BigDecimal monthlyFee) {
		accountNumber = new BigInteger(generateAccountNumber());
		this.balance = BigDecimal.ZERO;
		this.monthlyFee = monthlyFee;
		this.status = AccountStatus.ACTIVE;
		
		//Add te user to the account
		accountOwners.add(user);
	}
	
	
	/**
	 * Handles depositing money into accounts. To be implemented by each account
	 * @param amount - amount to deposit into the account (in form of $(#+).##)
	 * @param user - the user requesting the deposit
	 * @return a new Transaction object containing the relevant information about the deposit
	 * @throws AmountInvalidException if the amount is not in the valid format
	 * @throws AccountInactiveException if the account is NOT in either "ACTIVE" or "OVERDRAWN" status
	 * {@link deposit}
	 */
	public abstract Transaction deposit(String amount, User user) throws AmountInvalidException, AccountInactiveException;
	
	/**
	 * Handles withdrawing money from accounts. To be implemented by each account
	 * @param amount - amount to be withdrawn from account (in form of $(#+).##)
	 * @param user - the user requesting the withdraw
	 * @return a new Transaction object containing the relevant information about the withdraw
	 * @throws AmountInvalidException Thrown if te account is not in the valid format OR cannot be withdrawn due to overdraft limitations / negative balance
	 * @throws AccountInactiveException Thrown if the account is NOT in either "ACTIVE" or "OVERDRAWN" status
	 * {@link withdraw}
	 */
	public abstract Transaction withdraw(String amount, User user) throws AmountInvalidException, AccountInactiveException;
	
	/**
	 * Calculates the monthly fee to be deducted from the account
	 * @return a BigDecimal containing the amount to be taken as the monthly fee
	 * {@link calculateMonthlyFee}
	 */
	public abstract BigDecimal calculateMonthlyFee();
	
	/**
	 * Generates a pseudo-random number based on the system time and a random seed
	 * @return a randomly generated 9-digit account number
	 */
	private String generateAccountNumber() {
		long time = System.nanoTime();
		double randomNum = Math.random() * 1000;
		long num = (long) (time * randomNum);
		
		String accNum = num + "";
		
		//Take first 9 digits of generated number
		accNum = accNum.substring(0, 9);
		return accNum;
	}
	
	/**
	 * Checks if the amount is a valid monetary amount, and converts to a BigDecimal to be used in calculations
	 * @param amount - A string containing the proposed amount
	 * @return a BigDecimal with the specified amount
	 * @throws AmountInvalidException if the amount does not meet the format of $(#+.).##
	 */
	public BigDecimal getValidAmount(String amount) throws AmountInvalidException {
		if(amount.matches("^\\d+\\.\\d{2}$")) {
			return new BigDecimal(amount);
		} else {
			throw new AmountInvalidException("Amount \"" + amount + "\" invalid. Should be in form $(#+).##");
		}
	}
	
	/**
	 * Determines if the users in the list fill the age requirements to open savings accounts.
	 * These requirements are: Between the ages of 17-23 OR above 12 with an additional user above 18.
	 * @param users - list of proposed authorized users
	 * @return true if users meet age requirements, false otherwise
	 * @throws AccountAgeException if the users do not meet the age requirements
	 */
	public boolean meetsAgeRequirements(List<User> users) throws AccountAgeException {
		
		boolean under12 = false;
		boolean under17 = false;
		boolean adultPresent = false;
		boolean over23 = false;
		
		for(User user : users) {
			int ageInYears = getUserAge(user);
			if(ageInYears < 12)
				under12 = true;
			else if(ageInYears < 17)
				under17 = true;
			else if(ageInYears > 23)
				over23 = true;
			else if(ageInYears >= 18)
				adultPresent = true;
		}
		
		//If any user is under 17 with no adult present, or any of the users are less than 12
		if( (under17 && !adultPresent) || under12)
			throw new AccountAgeException("One or more users are too young and do not meet eligibility requirements for this account.");
		
		//If any user is over the age of 23 without having a minor attached to the account
		if( (!under17 && !under12) && over23)
			throw new AccountAgeException("One or more users are too old and do not meet eligibility requirements for this account.");
		
		return true;
		
	}
	
	/**
	 * Gets the provided users age
	 * @param user - user to get age from
	 * @return the users age in years
	 */
	public int getUserAge(User user) {
		LocalDate today = LocalDate.now();
		LocalDate userAge = user.getBirthDate();
		return Period.between(userAge, today).getYears();
	}
	
	/* GETTERS AND SETTERS */
	public BigInteger getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(BigInteger accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public List<User> getAccountOwners() {
		return accountOwners;
	}

	public void setAccountOwners(List<User> accountOwners) {
		this.accountOwners = accountOwners;
	}

	public BigDecimal getMonthlyFee() {
		return monthlyFee;
	}

	public void setMonthlyFee(BigDecimal monthlyFee) {
		this.monthlyFee = monthlyFee;
	}
	
	public AccountStatus getAccountStatus() {
		return status;
	}
	
	public void setAccountStatus(AccountStatus status) {
		this.status = status;
	}

}
