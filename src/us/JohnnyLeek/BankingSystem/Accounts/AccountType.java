package us.JohnnyLeek.BankingSystem.Accounts;

/**
 * Contains all account types
 * @author leek1j
 *
 */
public enum AccountType {

	BUSINESS_CHECKING,
	BUSINESS_SAVINGS,
	PERSONAL_CHECKING,
	PERSONAL_SAVINGS,
	STUDENT_CHECKING,
	STUDENT_SAVINGS
	
}
