package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.DAOs.TransactionDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Transactions.TransactionType;
import us.JohnnyLeek.BankingSystem.Users.User;
/**
 * Implementation of a Business Checking Account
 * Requirements:
 * 	- No opening restrictions
 * 	- No minimum balance
 * 	- $35 overdraft fee. Maximum overdraft of $7500
 *  - Monthly fee of $25 unless balance at end of month if $2500
 *  - If number of transactions exceeds 100 per month, each subsequent transaction will deduct $0.25 from the account
 * @author leek1j
 *
 */
public class BusinessCheckingAccount extends CheckingAccount {
	
	/**
	 * Constructor for opening with multiple users
	 * @param accountOwners - List of authorized users
	 * @throws UserExistingAccountException if one or more users already owns an account of this type
	 */
	public BusinessCheckingAccount(List<User> accountOwners) throws UserExistingAccountException {
		super(accountOwners, new BigDecimal("35"), new BigDecimal("-7500"), false, new BigDecimal("25"));
		
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			for(User user : accountOwners) {
				List<Account> accounts;
				accounts = accountDAO.getAccounts(user.getLicenseNumber());
				
				for(Account account : accounts) {
					if(account instanceof BusinessCheckingAccount)
						throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
				}
				
			}
		} catch(DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * Constructor for opening with one user
	 * @param user - User to own account
	 * @throws UserExistingAccountException if user already owns an account of this type
	 */
	public BusinessCheckingAccount(User user) throws UserExistingAccountException {
		super(user, new BigDecimal("35"), new BigDecimal("-7500"), false, new BigDecimal("25"));
		List<Account> accounts;
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			accounts = accountDAO.getAccounts(user.getLicenseNumber());
			for(Account account : accounts) {
				if(account instanceof BusinessCheckingAccount)
					throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
			}
		} catch (DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * @see #deposit(String)
	 */
	@Override
	public Transaction deposit(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validDepositAmt = super.getValidAmount(amount);
		BigDecimal depositResult = super.getBalance().add(validDepositAmt);
		
		//Set the balance
		super.setBalance(depositResult);
		
		//If the users account is overdrawn, and this transaction brings their account into the positive range, set it back to active
		if(super.getAccountStatus() == AccountStatus.OVERDRAWN) {
			if(depositResult.compareTo(BigDecimal.ZERO) >= 0) {
				super.setAccountStatus(AccountStatus.ACTIVE);
				System.out.println("Account " + super.getAccountNumber() + " is no longer overdrawn.");
			}
		}
		
		//If the total transactions for the month exceeds 100, deduct $0.25
		try {
			LocalDate begMonth = LocalDate.now().withDayOfMonth(1);
			LocalDate endMonth = LocalDate.now().plusMonths(1).minusDays(1);
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			int totalTransactions = transactionDAO.getAllTransactionsAccountRange(super.getAccountNumber(), begMonth, endMonth).size();
				if( totalTransactions > 100 ) {
				System.out.println("Too many transactions in the past month. Deducting $0.25");
				super.setBalance(super.getBalance().subtract(new BigDecimal("0.25")));
			}
			
			return new Transaction(validDepositAmt, depositResult, TransactionType.DEPOSIT, super.getAccountNumber(), user.getLicenseNumber());
		} catch (DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			System.exit(0);
		}
		return null;
	}
	
	/**
	 * @see #withdraw(String)
	 */
	@Override
	public Transaction withdraw(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validWithdrawAmt = super.getValidAmount(amount);
		BigDecimal withdrawResult = super.getBalance().subtract(validWithdrawAmt);
	
		//If the user hasn't opted into overdrafting, and this withdraw would bring balance below 0, reject transaction
		if(withdrawResult.compareTo(BigDecimal.ZERO) < 0 && !super.isCanOverdraft()) {
			throw new AmountInvalidException("Account " + super.getAccountNumber() + " has not opted in for overdrafting.");
		}
		
		if(withdrawResult.compareTo(super.getMaximumOverDraft()) >= 0) {
			//If this transaction would bring the user below $0, set status to "Overdrawn"
			if(withdrawResult.compareTo(BigDecimal.ZERO) < 0) {
				withdrawResult = withdrawResult.subtract(super.getOverDraftFee());
				if(super.getAccountStatus() != AccountStatus.OVERDRAWN) {
					super.setAccountStatus(AccountStatus.OVERDRAWN);
					System.out.println("Account " + super.getAccountNumber() + " is now overdrawn.");
				}
			}
			
			//Set the balance
			super.setBalance(withdrawResult);
			
			//If the total transactions for the month exceeds 100, deduct $0.25
			try {
				LocalDate begMonth = LocalDate.now().withDayOfMonth(1);
				LocalDate endMonth = LocalDate.now().plusMonths(1).minusDays(1);
				TransactionDAO transactionDAO = TransactionDAO.getInstance();
				int totalTransactions = transactionDAO.getAllTransactionsAccountRange(super.getAccountNumber(), begMonth, endMonth).size();
		
				if( totalTransactions > 100 ) {
					System.out.println("Too many transactions in the past month. Deducting $0.25");
					super.setBalance(super.getBalance().subtract(new BigDecimal("0.25")));
				}
				
				
				return new Transaction(validWithdrawAmt, withdrawResult, TransactionType.WITHDRAW, super.getAccountNumber(), user.getLicenseNumber());
			} catch(DAOReadException | DAOWriteException e) {
				System.out.println("Something went wrong. Terminating...");
				e.printStackTrace();
				System.exit(0);
			}
		}
		throw new AmountInvalidException("Amount would exceed overdraft limit.");
	}
	
	/**
	 * @see #calculateMonthlyFee()
	 */
	@Override
	public BigDecimal calculateMonthlyFee() {
		if(super.getBalance().compareTo(new BigDecimal("2500")) < 0)
			return super.getMonthlyFee();
		return BigDecimal.ZERO;
	}
	
	/**
	 * Prints the account in a English readable format
	 */
	@Override
	public String toString() {
		return "Business Checking Account: \n" +
			   "Account Number: " + super.getAccountNumber() + "\n" +
			   "Balance: " + super.getBalance() + "\n" +
			   "Account Owner: " + super.getAccountOwners() + "\n" +
			   "Monthly Fee: " + super.getMonthlyFee() + "\n" +
			   "Can Overdraft: " + super.isCanOverdraft() + "\n" +
			   "Status: " + super.getAccountStatus() + "\n\n";
	}
	
}
