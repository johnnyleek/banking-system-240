package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.util.List;

import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Transactions.TransactionType;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * Implementation of a Personal Savings Account
 * Requirements:
 * 	- No opening restrictions
 * 	- $25 minimum balance
 * 	- No overdrafting
 *  - Monthly fee of $10 unless balance at end of month if $500
 * @author leek1j
 *
 */
public class PersonalSavingsAccount extends SavingsAccount {

	/**
	 * Constructor for opening with multiple users
	 * @param accountOwners - List of authorized users
	 * @throws UserExistingAccountException if one or more users already owns an account of this type
	 */
	public PersonalSavingsAccount(List<User> accountOwners) throws UserExistingAccountException {
		super(accountOwners, new BigDecimal("25"), new BigDecimal("10"));
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			for(User user : accountOwners) {
				List<Account> accounts;
				accounts = accountDAO.getAccounts(user.getLicenseNumber());
				
				for(Account account : accounts) {
					if(account instanceof PersonalSavingsAccount)
						throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
				}
				
			}
		} catch(DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * Constructor for opening with one user
	 * @param user - User to open account
	 * @throws UserExistingAccountException if user already owns an account of this type
	 */
	public PersonalSavingsAccount(User user) throws UserExistingAccountException {
		super(user, new BigDecimal("25"), new BigDecimal("10"));
		List<Account> accounts;
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			accounts = accountDAO.getAccounts(user.getLicenseNumber());
			for(Account account : accounts) {
				if(account instanceof PersonalSavingsAccount)
					throw new UserExistingAccountException("User " + user.getLicenseNumber() + " already has an account of this type.");
			}
		} catch (DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
		}
	}
	
	/**
	 * @see #deposit(String)
	 */
	@Override
	public Transaction deposit(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validDepositAmt = super.getValidAmount(amount);
		BigDecimal depositResult = super.getBalance().add(validDepositAmt);
		
		//Set the balance
		super.setBalance(depositResult);
		
		return new Transaction(validDepositAmt, depositResult, TransactionType.DEPOSIT, super.getAccountNumber(), user.getLicenseNumber());
	}
	
	@Override
	public Transaction withdraw(String amount, User user) throws AmountInvalidException, AccountInactiveException {
		//If the account status is not equal to ACTIVE or OVERDRAWN
		if(super.getAccountStatus() != AccountStatus.ACTIVE && super.getAccountStatus() != AccountStatus.OVERDRAWN)
			throw new AccountInactiveException("This account is inaccessible at this time.");
		
		//Get valid amounts
		BigDecimal validWithdrawAmt = super.getValidAmount(amount);
		BigDecimal withdrawResult = super.getBalance().subtract(validWithdrawAmt);
		
		//If withdraw exceeds minimum balance
		if(withdrawResult.compareTo(super.getMinimumBalance()) < 0) {
			throw new AmountInvalidException("Withdraw Amount: $" + validWithdrawAmt + " would drop account " + super.getAccountNumber() + " below required minimum balance of $" + super.getMinimumBalance());
		}
		
		//Set the balance
		super.setBalance(withdrawResult);
		
		return new Transaction(validWithdrawAmt, withdrawResult, TransactionType.WITHDRAW, super.getAccountNumber(), user.getLicenseNumber());
	}
	
	/**
	 * Requirements:
	 *   A monthly fee of $10 shall be assessed from the account unless the balance at
	 *	 the end of the month is at least $500.
	 * @see #calculateMonthlyFee()
	 */
	@Override
	public BigDecimal calculateMonthlyFee() {
		if(super.getBalance().compareTo(new BigDecimal("500")) >= 0)
			return BigDecimal.ZERO;
		return super.getMonthlyFee();
	}
	
	/**
	 * Prints the account in a English readable format
	 */
	@Override
	public String toString() {
		return "Student Savings Account: \n" +
			   "Account Number: " + super.getAccountNumber() + "\n" +
			   "Balance: $" + super.getBalance() + "\n" +
			   "Account Owner: " + super.getAccountOwners() + "\n" +
			   "Monthly Fee: $" + super.getMonthlyFee() + "\n" +
			   "Minimum Balance: " + super.getMinimumBalance() + "\n" +
			   "Status: " + super.getAccountStatus() + "\n\n";
	}
	
}
