package us.JohnnyLeek.BankingSystem.Accounts;

import java.math.BigDecimal;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class serves as a template for a SavingsAccount. Extends from @see {@link Account}
 * Global Fields:
 * 		- minimumBalance: Minimum balance for the account
 * @author leek1j
 *
 */
public abstract class SavingsAccount extends Account {
	
	private BigDecimal minimumBalance;
	
	/**
	 * Constructor for multiple authorized users
	 * @param accountOwners - list of users that own account
	 * @param minimumBalance - minimum balance for account
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public SavingsAccount(List<User> accountOwners, BigDecimal minimumBalance, BigDecimal monthlyFee) {
		super(accountOwners, monthlyFee);
		this.minimumBalance = minimumBalance;
	}
	
	/**
	 * Constructor or only one authorized user
	 * @param user - owner of account
	 * @param minimumBalance - minimum balance for account
	 * @param monthlyFee - monthly fee associated with the account
	 */
	public SavingsAccount(User user, BigDecimal minimumBalance, BigDecimal monthlyFee) {
		super(user, monthlyFee);
		this.minimumBalance = minimumBalance;
	}

	/* GETTERS AND SETTERS */
	public BigDecimal getMinimumBalance() {
		return minimumBalance;
	}

	public void setMinimumBalance(BigDecimal minimumBalance) {
		this.minimumBalance = minimumBalance;
	}
	
	
}
