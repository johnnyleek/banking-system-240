package us.JohnnyLeek.BankingSystem.Transactions;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import us.JohnnyLeek.BankingSystem.DAOs.UserDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class serves as the Transaction model, handling business logic
 * and data storage for transactions.
 * Global Fields:
 * 		- transactionNumber: Transaction number for the transaction. 9 digit randomly generated BigInteger
 * 		- date: The date the transaction was generated
 * 		- time: The time the transaction was generated
 * 		- amount: The amount of the transaction
 * 		- newBalance: The account balance after the transaction occurred
 * 		- transactionType: Whether the account was a DEPOSIT or a WITHDRAW
 * 		- accountNumber: The account number the transaction was performed upon
 * 		- userLicense: The drivers license number of the user who performed the transaction
 * @author leek1j
 *
 */
public class Transaction implements Serializable{

	private BigInteger transactionNumber;
	private LocalDate date;
	private LocalTime time;
	private BigDecimal amount;
	private BigDecimal newBalance;
	private TransactionType transactionType;
	private BigInteger accountNumber;
	private String userLicense;
	
	/**
	 * Constructor to generate transaction. Date and time are generated upon instantiation,
	 * other fields are passed in.
	 * @param amount - amount of the transaction
	 * @param newBalance - the balance after the transaction occurred
	 * @param transactionType - whether the account was a DEPOSIT or a WITHDRAW
	 * @param accountNumber - the account number the transaction was performed upon
	 * @param userLicense - the drivers license number of the user who performed the transaction
	 */
	public Transaction(BigDecimal amount, BigDecimal newBalance, TransactionType transactionType, BigInteger accountNumber, String userLicense) {
		this.transactionNumber = new BigInteger(generateTransactionNum());
		this.date = LocalDate.now();
		this.time = LocalTime.now();
		this.amount = amount;
		this.newBalance = newBalance;
		this.transactionType = transactionType;
		this.accountNumber = accountNumber;
		this.userLicense = userLicense;
	}
	
	/**
	 * Generates a pseudo-random number based on the system time and a random seed
	 * @return a randomly generated 9-digit account number
	 */
	private String generateTransactionNum() {
		long time = System.nanoTime();
		double randomNum = Math.random() * 1000;
		long num = (long) (time * randomNum);
		
		String transNum = num + "";
		transNum = transNum.substring(0, 9);
		return transNum;
	}
	
	/**
	 * Returns an english readable representation of the transaction (assuming no database errors occur)
	 */
	@Override
	public String toString() {
		try {
		UserDAO userDAO = UserDAO.getInstance();
		User user = userDAO.getUser(getUserLicense());
		return "Transaction " + getTransactionNumber() + " | User: " + user.getFirstName() + " " + user.getMiddleName() + " " + user.getLastName() + " (" + user.getLicenseNumber() + ") | Date: " + getDateAsString() + " | Type: " + getTransactionType() + " | Amount: " + getAmount() + " | New Balance: " + getNewBalance();
		} catch(DAOReadException | DAOWriteException | UserNotFoundException e) {
			System.out.println("Something went wrong...");
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Formats the date and time as a string
	 * @return a readable datetime string in the format: "Month Day, Year at Hour:Minute:Second AM/PM Timezone"
	 */
	public String getDateAsString() {
		ZonedDateTime zonedDateTime = ZonedDateTime.of(date, time, ZoneId.of("America/New_York"));
		return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG).format(zonedDateTime);
	}
	
	/* GETTERS AND SETTERS */
	public BigInteger getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(BigInteger transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getNewBalance() {
		return newBalance;
	}
	public void setNewBalance(BigDecimal amount) {
		this.newBalance = amount;
	}
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	public BigInteger getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(BigInteger accountNumber) {
		this.accountNumber = accountNumber;
	}
	public void setUserLicense(String userLicense) {
		this.userLicense = userLicense;
	}
	public String getUserLicense() {
		return userLicense;
	}
	
	
	
}
