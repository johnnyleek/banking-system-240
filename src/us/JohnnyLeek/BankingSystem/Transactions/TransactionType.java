package us.JohnnyLeek.BankingSystem.Transactions;

/**
 * Houses the different transaction types.
 * @author leek1j
 *
 */
public enum TransactionType {

	DEPOSIT,
	WITHDRAW
	
}
