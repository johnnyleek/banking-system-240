package us.JohnnyLeek.BankingSystem.Controllers;

import java.math.BigInteger;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Accounts.Account;
import us.JohnnyLeek.BankingSystem.Accounts.AccountType;
import us.JohnnyLeek.BankingSystem.Accounts.BusinessCheckingAccount;
import us.JohnnyLeek.BankingSystem.Accounts.BusinessSavingsAccount;
import us.JohnnyLeek.BankingSystem.Accounts.PersonalCheckingAccount;
import us.JohnnyLeek.BankingSystem.Accounts.PersonalSavingsAccount;
import us.JohnnyLeek.BankingSystem.Accounts.StudentCheckingAccount;
import us.JohnnyLeek.BankingSystem.Accounts.StudentSavingsAccount;
import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountAgeException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountCloseException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserExistingAccountException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class is interacted with by the user, and communicates with the Account DAO to perform actions related to
 * the listing and management of accounts
 * @author leek1j
 *
 */
public class AccountListController {
	
	//AccountDAO reference
	private AccountDAO accountDAO;
	
	/**
	 * Constructor
	 * @param accountDAO - the Account DAO
	 */
	public AccountListController(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
		System.out.println("Account List Controller Initialized.");
	}
	
	/**
	 * Adds an account based on user input (For a single user)
	 * @param type - Type of account to open
	 * @param owner - User to open the account
	 * @return true if account is created successfully
	 * @throws AccountAgeException if User does not meet age requirements to open requested account
	 * @throws IllegalArgumentException if requested account type is invalid/hasn't been implemented 
	 * @throws UserExistingAccountException if User already owns an account of requested type
	 */
	public boolean addAccount(AccountType type, User owner) throws AccountAgeException, IllegalArgumentException, UserExistingAccountException {
		switch(type) {
			case BUSINESS_CHECKING:
				Account businessChecking = new BusinessCheckingAccount(owner);
				accountDAO.addAccount(businessChecking);
				break;
			case BUSINESS_SAVINGS:
				Account businessSavings = new BusinessSavingsAccount(owner);
				accountDAO.addAccount(businessSavings);
				break;
			case PERSONAL_CHECKING:
				Account personalChecking = new PersonalCheckingAccount(owner);
				accountDAO.addAccount(personalChecking);
				break;
			case PERSONAL_SAVINGS:
				Account personalSavings = new PersonalSavingsAccount(owner);
				accountDAO.addAccount(personalSavings);
				break;
			case STUDENT_CHECKING:
				Account studentChecking = new StudentCheckingAccount(owner);
				accountDAO.addAccount(studentChecking);
				break;
			case STUDENT_SAVINGS:
				Account studentSavings = new StudentSavingsAccount(owner);
				accountDAO.addAccount(studentSavings);
				break;
			default:
				throw new IllegalArgumentException("Account type invalid.");
		}
		return true;
	}
	
	/**
	 * Adds an account based on user input (For multiple user)
	 * @param type - Type of account to open
	 * @param owner - List of users to own account
	 * @return true if account is created successfully
	 * @throws AccountAgeException if users do not meet age requirements to open requested account
	 * @throws IllegalArgumentException if requested account type is invalid/hasn't been implemented 
	 * @throws UserExistingAccountException if one or more users already own an account of requested type
	 */
	public boolean addAccount(AccountType type, List<User> owners) throws AccountAgeException, IllegalArgumentException, UserExistingAccountException {
		switch(type) {
		case BUSINESS_CHECKING:
			Account businessChecking = new BusinessCheckingAccount(owners);
			accountDAO.addAccount(businessChecking);
			break;
		case BUSINESS_SAVINGS:
			Account businessSavings = new BusinessSavingsAccount(owners);
			accountDAO.addAccount(businessSavings);
			break;
		case PERSONAL_CHECKING:
			Account personalChecking = new PersonalCheckingAccount(owners);
			accountDAO.addAccount(personalChecking);
			break;
		case PERSONAL_SAVINGS:
			Account personalSavings = new PersonalSavingsAccount(owners);
			accountDAO.addAccount(personalSavings);
			break;
		case STUDENT_CHECKING:
			Account studentChecking = new StudentCheckingAccount(owners);
			accountDAO.addAccount(studentChecking);
			break;
		case STUDENT_SAVINGS:
			Account studentSavings = new StudentSavingsAccount(owners);
			accountDAO.addAccount(studentSavings);
			break;
		default:
			throw new IllegalArgumentException("Account type invalid.");
	}
	return true;
	}
	
	/**
	 * Closes a specific account
	 * @param accountNumber - account number of account to close
	 * @param user - user requesting account closure
	 * @return true if account closed successfully
	 * @throws AccountNotFoundException if account with provided number cannot be found
	 */
	public boolean closeAccount(BigInteger accountNumber, User user) throws AccountNotFoundException, AccountCloseException {
		
		accountDAO.closeAccount(accountNumber, user);
		return true;
		
	}
	
	/**
	 * Sets account status to Active after being closed
	 * @param accountNumber - account number to reopen
	 * @return true if account was opened successfully
	 * @throws AccountNotFoundException if account with provided number cannot be found
	 */
	public boolean openAccount(BigInteger accountNumber) throws AccountNotFoundException {
		
		accountDAO.openAccount(accountNumber);
		return true;
	
	}
	
	/**
	 * Gets an account with provided account number
	 * @param accountNumber - account number of account to retrieve
	 * @return Account object of specified account
	 * @throws AccountNotFoundException if account with provided number cannot be found
	 */
	public Account getAccount(BigInteger accountNumber) throws AccountNotFoundException {
		
		return accountDAO.getAccount(accountNumber);
		
	}
	
	/**
	 * Gets list of all accounts for a given user
	 * @param licenseNumber - drivers license number of user to get account from
	 * @return List of accounts that user owns
	 * @throws UserNotFoundException if user with provided number cannot be found
	 * @throws AccountNotFoundException a given account cannot be found based on retrieved account numbers
	 */
	public List<Account> getAccounts(String licenseNumber) throws UserNotFoundException, AccountNotFoundException {
		
		try {
			return accountDAO.getAccounts(licenseNumber);
		} catch (DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong. Terminating...");
			e.printStackTrace();
			return null;
		}
		
	}
	
	
}
