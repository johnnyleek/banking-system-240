package us.JohnnyLeek.BankingSystem.Controllers;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import us.JohnnyLeek.BankingSystem.DAOs.TransactionDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.TransactionNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;

/**
 * This class is interacted with by the user, and communicates with the Transaction DAO to perform actions related to
 * handling Transactions
 * @author leek1j
 *
 */
public class TransactionController {

	private TransactionDAO transactionDAO;
	
	public TransactionController(TransactionDAO transactionDAO) {
		this.transactionDAO = transactionDAO;
		System.out.println("Transaction Controller Initialized.");
	}
	
	/**
	 * Writes a transaction object to the database
	 * @param transaction - transaction to add
	 * @return true if transaction added successfully
	 */
	public boolean addTransaction(Transaction transaction) {
		
		transactionDAO.addTransaction(transaction);
		return true;
		
	}
	
	/**
	 * Gets a transaction with a specified transaction number
	 * @param transactionNumber - transaction number of requested transaction
	 * @return the retrieved transaction
	 * @throws TransactionNotFoundException if transaction with provided number cannot be found
	 */
	public Transaction getTransaction(BigInteger transactionNumber) throws TransactionNotFoundException {
		
		return transactionDAO.getTransaction(transactionNumber);
		
	}
	
	/**
	 * Gets all transactions from all accounts on a given date
	 * @param date - date to get transactions from
	 * @return a list of all transactions performed on specified date
	 */
	public List<Transaction> getAllTransactionsDate(LocalDate date) {
		
		return transactionDAO.getAllTransactionsDate(date);
	
	}
	
	/**
	 * Gets all transactions from all accounts in a given range
	 * @param startDate - Inclusive start date
	 * @param endDate - Inclusuve end date
	 * @return a list of all transactions from specified range
	 */
	public List<Transaction> getAllTransactionsRange(LocalDate startDate, LocalDate endDate) {
		
		return transactionDAO.getAllTransactionsRange(startDate, endDate);
	
	}
	
	/**
	 * Gets all transactions from a given account
	 * @param accountNumber - Account number to get transactions from
	 * @return a list of all transactions from a specified account
	 */
	public List<Transaction> getAllTransactionsAccount(BigInteger accountNumber) {
		
		return transactionDAO.getAllTransactionsAccount(accountNumber);
		
	}
	
	/**
	 * Gets all transactions from a given account on a given date
	 * @param accountNumber - Account number to get transactions from
	 * @param date - Date to get transactions from
	 * @return a list of all transactions from a specified account on a specified date
	 */
	public List<Transaction> getAllTransactionsAccountDate(BigInteger accountNumber, LocalDate date) {
		
		return transactionDAO.getAllTransactionsAccountDate(accountNumber, date);
	
	}
	
	/**
	 * Gets all transactions from a given account in a given range
	 * @param accountNumber - Account number to get transactions from
	 * @param startDate - Inclusive start date
	 * @param endDate - Inclusive end date
	 * @return a list of all transactions from a specified account from specified range
	 */
	public List<Transaction> getAllTransactionsAccountRange(BigInteger accountNumber, LocalDate startDate, LocalDate endDate) {
		
		return transactionDAO.getAllTransactionsAccountRange(accountNumber, startDate, endDate);
		
	}
	
}
