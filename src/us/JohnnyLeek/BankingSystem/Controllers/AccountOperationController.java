package us.JohnnyLeek.BankingSystem.Controllers;

import java.math.BigDecimal;

import us.JohnnyLeek.BankingSystem.Accounts.Account;
import us.JohnnyLeek.BankingSystem.DAOs.AccountDAO;
import us.JohnnyLeek.BankingSystem.DAOs.TransactionDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountInactiveException;
import us.JohnnyLeek.BankingSystem.Exceptions.AmountInvalidException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class is interacted with by the user, and communicates with the Account DAO to perform actions related to
 * the operating on a given account
 * @author leek1j
 *
 */
public class AccountOperationController {
	
	public AccountOperationController() {
		System.out.println("Account Operation Controller Initialized.");
	}
	
	/**
	 * Withdraws money from specified account
	 * @param account - Account to withdraw from
	 * @param amount - Amount to withdraw
	 * @param user - User requesting the withdraw
	 * @return new balance of account after withdraw
	 * @throws AmountInvalidException if provided amount is invalid
	 * @throws AccountInactiveException if provided account is not "ACTIVE" or "OVERDRAWN"
	 */
	public BigDecimal withdraw(Account account, String amount, User user) throws AmountInvalidException, AccountInactiveException {
		
		
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			transactionDAO.addTransaction(account.withdraw(amount, user));
			accountDAO.saveAccounts();
		} catch(DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong...");
			e.printStackTrace();
		}
		
		return account.getBalance();

	}
	
	/**
	 * Deposits money into specified account
	 * @param account - Account to deposit into
	 * @param amount - Amount to deposit
	 * @param user - User requesting the deposit
	 * @return new balance of account after deposit
	 * @throws AmountInvalidException if provided amount is invalid
	 * @throws AccountInactiveException if provided account is not "ACTIVE" or "OVERDRAWN"
	 */
	public BigDecimal deposit(Account account, String amount, User user) throws AmountInvalidException, AccountInactiveException {
		
		try {
			AccountDAO accountDAO = AccountDAO.getInstance();
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			transactionDAO.addTransaction(account.deposit(amount, user));
			accountDAO.saveAccounts();
		} catch(DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong...");
			e.printStackTrace();
		}
		return account.getBalance();
		
	}
	
	public BigDecimal getAccountBalance(Account account) {
		return account.getBalance();
	}
	
}
