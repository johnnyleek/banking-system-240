package us.JohnnyLeek.BankingSystem.Controllers;

import java.time.LocalDate;

import us.JohnnyLeek.BankingSystem.DAOs.UserDAO;
import us.JohnnyLeek.BankingSystem.Exceptions.UserAlreadyExistsException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Users.Address;
import us.JohnnyLeek.BankingSystem.Users.AddressState;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * This class is interacted with by the user, and communicates with the User DAO to perform actions related to
 * handling Users
 * @author leek1j
 *
 */
public class UserController {

	private UserDAO userDAO;
	
	public UserController(UserDAO userDAO) {
		this.userDAO = userDAO;
		System.out.println("User Controller Initialized.");
	}
	
	/**
	 * Adds a user to the database, assuming it has valid information
	 * @param firstName - Users first name
	 * @param middleName - Users middle name
	 * @param lastName - Users last name
	 * @param birthDate - Users birth date
	 * @param licenseNumber - Users drivers license
	 * @param occupation - Users occupation
	 * @param houseNum - Users house number
	 * @param apartmentNum - Users apartment number
	 * @param street - Users street
	 * @param city - Users city
	 * @param state - Users state
	 * @param zipCode - Users zip code
	 * @throws IllegalArgumentException if one or more pieces of information is invalid or incomplete
	 * @throws UserAlreadyExistsException if the user with the provided license number already exists
	 */
	public void addUser(String firstName, String middleName, String lastName, LocalDate birthDate,
						String licenseNumber, String occupation, int houseNum, int apartmentNum,
						String street, String city, AddressState state, int zipCode) throws IllegalArgumentException, UserAlreadyExistsException {
		
		if(validInput(firstName, middleName, lastName, birthDate, licenseNumber, occupation, houseNum,
					  apartmentNum, street, city, state, zipCode)) {
			User user = new User(firstName, middleName, lastName, birthDate, licenseNumber, occupation,
								 new Address(licenseNumber, houseNum, apartmentNum, street, city, state, zipCode));
			
			if(userDAO.userExists(licenseNumber)) {
				throw new UserAlreadyExistsException("User already exists.");
			}
			
			userDAO.addUser(user);
		} else {
			throw new IllegalArgumentException("Could not add user.");
		}
		
	}
	
	/**
	 * Remove a user with a provided license number
	 * @param licenseNumber - license number of user to remove
	 * @throws UserNotFoundException if user with provided license number cannot be found
	 */
	public void removeUser(String licenseNumber) throws UserNotFoundException {
		
		userDAO.removeUser(licenseNumber);
		
	}
	
	/**
	 * Gets a specified user by license number
	 * @param licenseNumber - license number of user to retrieve from DAO
	 * @return the User object of the retrieved user
	 * @throws UserNotFoundException if user with provided license number cannot be found
	 */
	public User getUser(String licenseNumber) throws UserNotFoundException {
		
		return userDAO.getUser(licenseNumber);
		
	}
	
	/**
	 * Updates a user with new information
	 * @param user - user object to update existing user with
	 * @return true if the update was successful
	 * @throws UserNotFoundException if the user with provided license number (inside user object) cannot be found
	 */
	public boolean updateUser(User user) throws UserNotFoundException {
		
		return userDAO.updateUser(user);
		
	}
	
	/**
	 * Checks for valid input
	 * @param firstName - Users first name
	 * @param middleName - Users middle name
	 * @param lastName - Users last name
	 * @param birthDate - Users birth date
	 * @param licenseNumber - Users drivers license
	 * @param occupation - Users occupation
	 * @param houseNum - Users house number
	 * @param apartmentNum - Users apartment number
	 * @param street - Users street
	 * @param city - Users city
	 * @param state - Users state
	 * @param zipCode - Users zip code
	 * @return true if input is valid
	 * @throws IllegalArgumentException if any information is incomplete or incorrect
	 */
	private boolean validInput(String firstName, String middleName, String lastName, LocalDate birthDate,
							   String licenseNumber, String occupation, int houseNum, int apartmentNum,
							   String street, String city, AddressState state, int zipCode) throws IllegalArgumentException {
		
		if (firstName == null || firstName.isEmpty()) {
			throw new IllegalArgumentException("First name is invalid.");
		}
		
		if(middleName == null) {
			throw new IllegalArgumentException("Middle name is invalid");
		}
		
		if(lastName == null) {
			throw new IllegalArgumentException("Last name is invalid.");
		}
		
		if(birthDate == null) {
			throw new IllegalArgumentException("DoB is invalid.");
		}
		
		//License Number must be a 9 digit number (Ex. 123456789 is valid, 12345678 is not)
		if(licenseNumber == null || !licenseNumber.matches("[0-9]{9}") ) {
			throw new IllegalArgumentException("License Number is invalid.");
		}
		
		if(occupation == null) {
			throw new IllegalArgumentException("Occupation is invalid.");
		}
		
		if(houseNum < 0) {
			throw new IllegalArgumentException("House Number is invalid.");
		}
		
		if(street == null) {
			throw new IllegalArgumentException("Street name is invalid.");
		}
		
		if(city == null) {
			throw new IllegalArgumentException("City name is invalid.");
		}
		
		if(state == null) {
			throw new IllegalArgumentException("State is invalid.");
		}
		
		//Zip Code must be 5 digits (Ex. 48858 is valid, 1234 is not)
		if(!Integer.toString(zipCode).matches("[0-9]{5}") || zipCode < 0) {
			throw new IllegalArgumentException("Zip code is invalid");
		}
		
		return true;
	}
	
}
