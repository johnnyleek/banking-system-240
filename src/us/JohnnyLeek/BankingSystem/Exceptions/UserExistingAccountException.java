package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a user tries to open a new account of a type
 * in which they already own
 * @author leek1j
 *
 */
public class UserExistingAccountException extends Exception {

	public UserExistingAccountException(String message) {
		super(message);
	}
	
}
