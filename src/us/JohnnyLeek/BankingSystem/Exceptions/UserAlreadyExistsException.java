package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever the system tries to create a user with an
 * existing drivers license number
 * @author leek1j
 *
 */
public class UserAlreadyExistsException extends Exception {

	public UserAlreadyExistsException(String message) {
		super(message);
	}
	
}
