package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a user cannot be found
 * @author leek1j
 *
 */
public class UserNotFoundException extends Exception {
	public UserNotFoundException(String message) {
		super(message);
	}
}
