package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever an address cannot be found
 * @author leek1j
 *
 */
public class AddressNotFoundException extends Exception {

	public AddressNotFoundException(String message) {
		super(message);
	}
	
}
