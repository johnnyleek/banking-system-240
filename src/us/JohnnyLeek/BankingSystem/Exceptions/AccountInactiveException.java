package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown when a user tries to perform an operation on an inactive
 * account (other than reopening or closing it)
 * to open a new account
 * @author leek1j
 *
 */
public class AccountInactiveException extends Exception {

	public AccountInactiveException(String message) {
		super(message);
	}
	
}
