package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever an IO issue occurs when reading from a DAO
 * @author leek1j
 *
 */
public class DAOWriteException extends Exception {

	public DAOWriteException(String message) {
		super(message);
	}
	
}
