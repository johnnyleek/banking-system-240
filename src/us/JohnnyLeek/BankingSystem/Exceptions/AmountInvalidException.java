package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a user tries to deposit or withdraw an invalid
 * amount
 * @author leek1j
 *
 */
public class AmountInvalidException extends Exception {

	public AmountInvalidException(String message) {
		super(message);
	}
	
}
