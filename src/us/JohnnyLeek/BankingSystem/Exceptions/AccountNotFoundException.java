package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever an account cannot be found
 * @author leek1j
 *
 */
public class AccountNotFoundException extends Exception {

	public AccountNotFoundException(String message) {
		super(message);
	}
	
}
