package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever an IO or a casting issue occurs when reading
 * from a DAO
 * @author leek1j
 *
 */
public class DAOReadException extends Exception {

	public DAOReadException(String message) {
		super(message);
	}
	
}
