package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a user cannot close an account
 * @author leek1j
 *
 */
public class AccountCloseException extends Exception {

	public AccountCloseException(String message) {
		super(message);
	}
	
}
