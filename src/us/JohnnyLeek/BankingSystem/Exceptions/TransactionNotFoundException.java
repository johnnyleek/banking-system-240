package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a transaction cannot be found
 * @author leek1j
 *
 */
public class TransactionNotFoundException extends Exception{

	public TransactionNotFoundException(String message) {
		super(message);
	}
	
}
