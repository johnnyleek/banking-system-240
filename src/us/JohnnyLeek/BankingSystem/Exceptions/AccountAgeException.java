package us.JohnnyLeek.BankingSystem.Exceptions;

/**
 * This exception is to be thrown whenever a user doesn't meet certain age requirements
 * to open a new account
 * @author leek1j
 *
 */
public class AccountAgeException extends Exception {

	public AccountAgeException(String message) {
		super(message);
	}
	
}
