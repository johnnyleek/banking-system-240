package us.JohnnyLeek.BankingSystem.DAOs;

import java.util.HashMap;

import us.JohnnyLeek.BankingSystem.Exceptions.AddressNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Users.Address;

/**
 * Implements a Data Access Object for addresses.
 * This DAO is a lazy singleton, meaning only one instance can exist as any
 * given time.
 * @author leek1j
 *
 */
public class AddressDAO extends DAO<String, Address>{

	private static AddressDAO instance = null;
	HashMap<String, Address> addresses = new HashMap<String, Address>();
	
	private int errors = 0;
	
	/**
	 * Tries to initialize the DAO. Attempts to run up to 10 times until it succeeds.
	 * If unsuccessful, it will attempt to fix itself.
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	private AddressDAO() throws DAOReadException, DAOWriteException {
		super("addresses.bank");
		
		//Initialize
		while(errors < 10) {
			try {
				addresses = super.getAll(super.getDB_Name());
				errors = 0;
				break;
			} catch(DAOReadException e) {
				super.autoFix(super.getDB_Name(), addresses);
			}
		}
		if(errors >= 10) {
			throw new DAOReadException("AddressDAO Failed to read users. Could not auto-fix");
		}
		
		System.out.println("Address DAO Initialized.");
	}
	
	/**
	 * Adds an address to the list of addresses
	 * @param address - address to add
	 * @return true if address added successfully
	 */
	public boolean addAddress(Address address) {
		addresses.put(address.getLicenseNumber(), address);
		saveAddresses();
		return true;
	}
	
	/**
	 * Gets address with specified drivers license
	 * @param driversLicense - drivers license of requested address
	 * @return Address object of retrieved address
	 * @throws AddressNotFoundException if address could not be found
	 */
	public Address getAddress(String driversLicense) throws AddressNotFoundException {
		if(!addresses.containsKey(driversLicense))
			throw new AddressNotFoundException("No address found with license number: " + driversLicense);

		return addresses.get(driversLicense);
		
	}
	
	/**
	 * Updates an address. Takes in an address to modify with the same license number. Replaces existing attributes with new attributes
	 * @param address - the address to modify with the new attributes applied
	 * @return true if the operation was successful
	 * @throws AddressNotFoundException if address with specified license number does not exist
	 */
	public boolean updateAddress(Address address) throws AddressNotFoundException {
		Address addressToUpdate = getAddress(address.getLicenseNumber());
		addressToUpdate.setApartmentNum(address.getApartmentNum());
		addressToUpdate.setHouseNum(address.getHouseNum());
		addressToUpdate.setState(address.getState());
		addressToUpdate.setStreet(address.getStreet());
		addressToUpdate.setZipCode(address.getZipCode());
		saveAddresses();
		return true;
	}
	
	/**
	 * Removes an address from the database.
	 * @param licenseNumber - the license number of the address to remove
	 * @return true if operation is successful
	 * @throws AddressNotFoundException if address with specified license number cannot be found
	 */
	public boolean removeAddress(String licenseNumber) throws AddressNotFoundException {
		
		Address addressToRemove = getAddress(licenseNumber);		
		addresses.remove(addressToRemove.getLicenseNumber());
		saveAddresses();
		return true;
		
	}
	
	/**
	 * Saves all data with current address data
	 * @return true if successful write to database, false if unsuccessful
	 */
	public boolean saveAddresses() {
		try {
			return super.saveAll(super.getDB_Name(), addresses);
		} catch(DAOWriteException e) {
			System.out.println("[ADDRESS DAO] Failed to save all addresses");
			return false;
		}
	}
	
	/**
	 * Retrieves all data from the database and populates address hashmap
	 * @return true if successful read from database, false if unsuccessful
	 */
	public boolean getAllAddresses() {
		try {
			addresses = super.getAll(super.getDB_Name());
			return true;
		} catch(DAOReadException e) {
			System.out.println("[ADDRESS DAO] Failed to read all addresses");
			return false;
		}
	}
	
	/**
	 * Gets current instance of DAO (This is a singleton class)
	 * @return the current instance
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	public static AddressDAO getInstance() throws DAOReadException, DAOWriteException {
		if(instance == null)
			instance = new AddressDAO();
		
		return instance;
	}
	
	
}
