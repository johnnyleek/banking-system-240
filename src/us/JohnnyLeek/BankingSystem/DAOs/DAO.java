package us.JohnnyLeek.BankingSystem.DAOs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;

/**
 * This is the parent DAO class. It handles reading and writing of files.
 * @author leek1j
 *
 */
public abstract class DAO<K, V> {

	private final String DB_NAME;
	
	/**
	 * Constructor to initialize DAO
	 * @param name - Name of Database
	 */
	public DAO(String name) {
		DB_NAME = name;
	}
	
	/**
	 * The most common issue when the DAO fails to read or write is if the file hasn't been created. To solve this,
	 * we delete and recreate the file.
	 * @param fileName - Database file name
	 * @param data - Hashmap containing data
	 * @throws DAOReadException if the file system continues to refuse to read
	 * @throws DAOWriteException if the file system continues to refuse to read
	 */
	public void autoFix(String fileName, HashMap<K, V> data) throws DAOReadException, DAOWriteException {
		System.out.println("[DAO] An error occurred with: " + fileName + ". (Could just be first initialization). Trying to auto-fix");
		createDB(fileName);
		saveAll(fileName, data);
	}
	
	/**
	 * Gets all data from the database and returns a hashmap with the corresponding data
	 * @param fileName - Database file name
	 * @return a hashmap populated with the serialized data in the database file
	 * @throws DAOReadException if there is an IO error or the hashmap is corrupted
	 */
	public HashMap<K, V> getAll(String fileName) throws DAOReadException {
		try {
			FileInputStream fileIn = new FileInputStream("data/" + fileName);
			ObjectInputStream objectIn = new ObjectInputStream(fileIn);
			HashMap<K, V> data = (HashMap<K, V>) objectIn.readObject();
			objectIn.close();
			fileIn.close();
			System.out.println("[DAO] Successfully read data from: data/" + fileName);
			return data;
		} catch(IOException e) {
			throw new DAOReadException("Failed to read data from: data/" + fileName + " (IO Error)");
		} catch(ClassNotFoundException e) {
			throw new DAOReadException("Failed to read data from: data/" + fileName + " (Class Error)");
		}
	}
	
	/**
	 * Serializes and saves data into a provided data file
	 * @param fileName - Database file name
	 * @param data - Data to save in Database
	 * @return true is write was successful
	 * @throws DAOWriteException if there is an IO error writing to the file
	 */
	public boolean saveAll(String fileName, HashMap<K, V> data) throws DAOWriteException {
		try {
			FileOutputStream fileOut = new FileOutputStream("data/" + fileName);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			
			objectOut.writeObject(data);
			
			objectOut.close();
			fileOut.close();
			System.out.println("[DAO] Successfully saved data to: data/" + fileName);
			return true;
		} catch(IOException e) {
			e.printStackTrace();
			throw new DAOWriteException("Failed to write data to: data/" + fileName + " (IO Error)");
		}
	}
	
	/**
	 * Creates a new data file if one doesn't exist, otherwise do nothing
	 * @param fileName - Database file name
	 * @return true if creation was successful
	 * @throws DAOWriteException if there is an IO error creating the file
	 */
	public boolean createDB(String fileName) throws DAOWriteException {
		try {
			File newDB = new File("data/" + fileName);
			
			if(newDB.createNewFile()) {
				System.out.println("[DAO] Successfully created new DB with name: " + fileName);
			} else {
				System.out.println("[DAO] Database named \"" + fileName + "\" already exists.");
			}
			
			return true;
		} catch(IOException e) {
			throw new DAOWriteException("Failed to create new DB with name: " + fileName + " (IO Error)");
		}
	}
	
	/* Getter and Setter */
	public String getDB_Name() {
		return DB_NAME;
	}
	
}
