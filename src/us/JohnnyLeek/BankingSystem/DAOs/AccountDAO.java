package us.JohnnyLeek.BankingSystem.DAOs;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import us.JohnnyLeek.BankingSystem.Accounts.Account;
import us.JohnnyLeek.BankingSystem.Accounts.AccountStatus;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountCloseException;
import us.JohnnyLeek.BankingSystem.Exceptions.AccountNotFoundException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;
import us.JohnnyLeek.BankingSystem.Transactions.TransactionType;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * Implements a Data Access Object for different types of accounts.
 * This DAO is a lazy singleton, meaning only one instance can exist as any
 * given time.
 * @author leek1j
 *
 */
public class AccountDAO extends DAO<BigInteger, Account>{

	private static volatile AccountDAO instance = null;
	HashMap<BigInteger, Account> accounts = new HashMap<BigInteger, Account>();
	
	private int errors = 0;
	
	/**
	 * Tries to initialize the DAO. Attempts to run up to 10 times until it succeeds.
	 * If unsuccessful, it will attempt to fix itself.
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	private AccountDAO() throws DAOReadException, DAOWriteException {
		super("accounts.bank");
		//Initialize DAO
		while(errors < 10) {
			try {
				accounts = super.getAll(super.getDB_Name());
				errors = 0;
				break;
			} catch(DAOReadException e) {
				super.autoFix(super.getDB_Name(), accounts);
			}
		}
		if(errors >= 10) {
			throw new DAOReadException("AccountDAO Failed to read accounts. Could not auto-fix");
		}
		
		System.out.println("Account DAO Initialized.");
	}
	
	/**
	 * Adds an account to the list of user accounts
	 * @param account - account to add
	 * @return true if account added successfully
	 */
	public boolean addAccount(Account account) {
		accounts.put(account.getAccountNumber(), account);
		try {
			UserDAO userDAO = UserDAO.getInstance();
			for(User user : account.getAccountOwners()) {
				List<BigInteger> currentAccNumbers = user.getAccountNumbers();
				currentAccNumbers.add(account.getAccountNumber());
				user.setAccountNumbers(currentAccNumbers);
			}
			userDAO.saveAll(userDAO.getDB_Name(), userDAO.users);
			saveAccounts();
			return true;
		} catch (DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong... Terminating program.");
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Gets account with specified account number
	 * @param accountNumber - account number of requested account
	 * @return Account object of retrieved account
	 * @throws AccountNotFoundException if account could not be found
	 */
	public Account getAccount(BigInteger accountNumber) throws AccountNotFoundException {
		if(!accounts.containsKey(accountNumber))
			throw new AccountNotFoundException("No account was found with account number: " + accountNumber);

		return accounts.get(accountNumber);
	}
	
	/**
	 * Gets all accounts tied to a specific user
	 * @param licenseNumber - user to get accounts from
	 * @return list of user accounts
	 * @throws DAOReadException if DAO cannot read from database
	 * @throws DAOWriteException if DAO cannot write to database
	 * @throws UserNotFoundException if user with specified license number cannot be found
	 */
	public List<Account> getAccounts(String licenseNumber) throws DAOReadException, DAOWriteException, UserNotFoundException{
		List<Account> userAccounts = new ArrayList<Account>();
		UserDAO userDAO = UserDAO.getInstance();
		User user = userDAO.getUser(licenseNumber);
		for(BigInteger accountNum : user.getAccountNumbers()) {
			try {
				userAccounts.add(getAccount(accountNum));
			} catch(AccountNotFoundException e) {
				continue;
			}
		}
		return userAccounts;
		
	}
	
	/**
	 * Updates an account. Takes in an account with the same account number. Replaces existing attributes with new attributes
	 * @param account - the account to modify with new attributes applied
	 * @return true if the operation is successful
	 * @throws AccountNotFoundException if account number specified does not exist
	 */
	public boolean updateAccount(Account account) throws AccountNotFoundException {
		Account accountToUpdate = getAccount(account.getAccountNumber());
		accountToUpdate.setBalance(account.getBalance());
		accountToUpdate.setAccountOwners(account.getAccountOwners());
		accountToUpdate.setMonthlyFee(account.getMonthlyFee());
		saveAccounts();
		return true;
	}
	
	/**
	 * Closes a given account. Note: This does not delete the account, rather set it at closed.
	 * Generates a transaction regarding the closure (A withdraw of the remaining account amount)
	 * @param accountNumber - the account to close
	 * @param user - the user requesting the account closure
	 * @return true if the account can successfully be closed
	 * @throws AccountNotFoundException if the given account cannot be found
	 * @throws AccountCloseException if the account cannot be closed (has outstanding balance to pay)
	 */
	public boolean closeAccount(BigInteger accountNumber, User user) throws AccountNotFoundException, AccountCloseException {
		if(!accounts.containsKey(accountNumber)) {
			throw new AccountNotFoundException("Cannot find account: " + accountNumber);
		}
		Account account = getAccount(accountNumber);
		
		if(account.getBalance().compareTo(BigDecimal.ZERO) < 0 || account.getAccountStatus() == AccountStatus.OVERDRAWN)
			throw new AccountCloseException("Cannot close account as this account is overdrawn. Pay remaining balance before closing.");
		
		try {
			TransactionDAO transactionDAO = TransactionDAO.getInstance();
			account.setAccountStatus(AccountStatus.CLOSED);
			Transaction emptyAccount = new Transaction(account.getBalance(), BigDecimal.ZERO, TransactionType.WITHDRAW, account.getAccountNumber(), user.getLicenseNumber());
			transactionDAO.addTransaction(emptyAccount);
			account.setBalance(BigDecimal.ZERO);
			saveAccounts();
			return true;
		} catch(DAOReadException | DAOWriteException e) {
			System.out.println("Something went wrong... Terminating");
		}
		return false;
	}
	
	/**
	 * Reopens account that was previously closed
	 * @param accountNumber - the account to open
	 * @return true if the acount can successfully be opened
	 * @throws AccountNotFoundException if the given account number cannot be found
	 */
	public boolean openAccount(BigInteger accountNumber) throws AccountNotFoundException {
		if(!accounts.containsKey(accountNumber)) {
			throw new AccountNotFoundException("Cannot find account: " + accountNumber);
		}
		
		Account account = getAccount(accountNumber);
		account.setAccountStatus(AccountStatus.ACTIVE);
		saveAccounts();
		return true;
	}
	
	/**
	 * Saves all data with current account data
	 * @return true if successful write to database, false if unsuccessful
	 */
	public boolean saveAccounts() {
		try {
			return super.saveAll(super.getDB_Name(), accounts);
		} catch(DAOWriteException e) {
			System.out.println("[ACCOUNT DAO] Failed to save all accounts");
			return false;
		}
	}
	
	/**
	 * Retrieves all data from the database and populates accounts hashmap
	 * @return true if successful read from database, false if unsuccessful
	 */
	public boolean getAllAccounts() {
		try {
			accounts = super.getAll(super.getDB_Name());
			return true;
		} catch(DAOReadException e) {
			System.out.println("[ACCOUNT DAO] Failed to read all accounts");
			return false;
		}
	}
	
	/**
	 * Gets current instance of DAO (This is a singleton class)
	 * @return the current instance
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	public static AccountDAO getInstance() throws DAOReadException, DAOWriteException {
		if(instance == null)
			instance = new AccountDAO();
		
		return instance;
			
	}
	
}
