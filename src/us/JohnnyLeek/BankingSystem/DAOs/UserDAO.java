package us.JohnnyLeek.BankingSystem.DAOs;

import java.util.HashMap;

import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.UserNotFoundException;
import us.JohnnyLeek.BankingSystem.Users.User;

/**
 * Implements a Data Access Object for users.
 * This DAO is a lazy singleton, meaning only one instance can exist as any
 * given time.
 * @author leek1j
 *
 */

public class UserDAO extends DAO<String, User>{
	
	private static volatile UserDAO instance = null;
	HashMap<String, User> users = new HashMap<String, User>();
	
	private int errors = 0;
	
	/**
	 * Tries to initialize the DAO. Attempts to run up to 10 times until it succeeds.
	 * If unsuccessful, it will attempt to fix itself.
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	private UserDAO() throws DAOReadException, DAOWriteException {
		super("users.bank");
		//Initialize DAO
		while(errors < 10) {
			try {
				users = super.getAll(super.getDB_Name());
				errors = 0;
				break;
			} catch(DAOReadException e) {
				super.autoFix(super.getDB_Name(), users);
			}
		}
		if(errors >= 10) {
			throw new DAOReadException("UserDAO Failed to read users. Could not auto-fix");
		}
		
		System.out.println("User DAO Initialized.");
		
	}
	
	/**
	 * Adds a user to the list of users
	 * @param user - user to add
	 * @return true if user added successfully
	 */
	public boolean addUser(User user) {
		users.put(user.getLicenseNumber(), user);
		saveUsers();
		return true;
	}
	
	/**
	 * Gets user with specified drivers license
	 * @param driversLicense - drivers license of requested user
	 * @return User object of retrieved user
	 * @throws UserNotFoundException if user could not be found
	 */
	public User getUser(String driversLicense) throws UserNotFoundException {		
		if(!users.containsKey(driversLicense))
			throw new UserNotFoundException("No user found with license number: " + driversLicense);
		
		return users.get(driversLicense);
	}
	
	/**
	 * Checks if a user exists
	 * @param driversLicense - drivers license number of user to check existence of
	 * @return true if user exists, false if user doesn't exist
	 */
	public boolean userExists(String driversLicense) {
		if(users.containsKey(driversLicense)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Updates a user. Takes in a user to modify with the same license number. Replaces existing attributes with new attributes
	 * @param user - the user to modify with the new attributes applied
	 * @return true if the operation was successful
	 * @throws UserNotFoundException if user with specified license number does not exist
	 */
	public boolean updateUser(User user) throws UserNotFoundException{
		
		User userToUpdate = getUser(user.getLicenseNumber());
		
		if(!users.containsKey(userToUpdate.getLicenseNumber()))
			throw new UserNotFoundException("Cannot find user");
		
		userToUpdate.setFirstName(user.getFirstName());
		userToUpdate.setMiddleName(user.getMiddleName());
		userToUpdate.setLastName(user.getLastName());
		userToUpdate.setBirthDate(user.getBirthDate());
		userToUpdate.setOccupation(user.getOccupation());
		saveUsers();
		return true;
	}
	
	/**
	 * Removes a user from the database
	 * @param licenseNumber - the license number of the user to remove
	 * @return true if the operation was successful
	 * @throws UserNotFoundException if user with specified drivers license doesn't exist
	 */
	public boolean removeUser(String licenseNumber) throws UserNotFoundException {
		User userToRemove = getUser(licenseNumber);
		users.remove(userToRemove.getLicenseNumber());
		saveUsers();
		return true;
		
	}
	
	/**
	 * Saves all data with current user data
	 * @return true if successful write to database, false if unsuccessful
	 */
	public boolean saveUsers() {
		try {
			AddressDAO addressDAO = AddressDAO.getInstance();
			addressDAO.saveAddresses();
			return super.saveAll(super.getDB_Name(), users);
		} catch(DAOWriteException | DAOReadException e) {
			System.out.println("[USER DAO] Failed to save all users");
			return false;
		}
	}
	
	/**
	 * Retrieves all data from the database and populates user hashmap
	 * @return true if successful read from database, false if unsuccessful
	 */
	public boolean getAllUsers() {
		try {
			users = super.getAll(super.getDB_Name());
			return true;
		} catch(DAOReadException e) {
			System.out.println("[USER DAO] Failed to read all users");
			return false;
		}
	}
	
	/**
	 * Gets current instance of DAO (This is a singleton class)
	 * @return the current instance
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	public static UserDAO getInstance() throws DAOReadException, DAOWriteException {
		if(instance == null)
			instance = new UserDAO();
		return instance;
	}
	
}

