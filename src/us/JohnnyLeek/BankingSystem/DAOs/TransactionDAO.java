package us.JohnnyLeek.BankingSystem.DAOs;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import us.JohnnyLeek.BankingSystem.Exceptions.DAOReadException;
import us.JohnnyLeek.BankingSystem.Exceptions.DAOWriteException;
import us.JohnnyLeek.BankingSystem.Exceptions.TransactionNotFoundException;
import us.JohnnyLeek.BankingSystem.Transactions.Transaction;

/**
 * Implements a Data Access Object for transactions.
 * This DAO is a lazy singleton, meaning only one instance can exist as any
 * given time.
 * @author leek1j
 *
 */
public class TransactionDAO extends DAO<BigInteger, Transaction>{

	private static volatile TransactionDAO instance = null;
	HashMap<BigInteger, Transaction> transactions = new HashMap<BigInteger, Transaction>();
	
	private int errors = 0;
	
	/**
	 * Tries to initialize the DAO. Attempts to run up to 10 times until it succeeds.
	 * If unsuccessful, it will attempt to fix itself.
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	private TransactionDAO() throws DAOReadException, DAOWriteException {
		super("transactions.bank");
		//Initialize DAO
		while(errors < 10) {
			try {
				transactions = super.getAll(super.getDB_Name());
				errors = 0;
				break;
			} catch(DAOReadException e) {
				super.autoFix(super.getDB_Name(), transactions);
			}
		}
		if(errors >= 10) {
			throw new DAOReadException("TransactionDAO Failed to read transactions. Could not autofix");
		}
		
		System.out.println("Transaction DAO Initialized.");
	}
	
	/**
	 * Adds a transaction to the list of transactions
	 * @param transaction - transaction object to add
	 * @return true if the transaction was added successfully
	 */
	public boolean addTransaction(Transaction transaction) {
		transactions.put(transaction.getTransactionNumber(), transaction);
		saveTransactions();
		return true;
	}
	
	/**
	 * Gets a transaction with a specified transaction number
	 * @param transactionNumber - Transaction number of requested transaction
	 * @return Transaction object of retrieved transaction
	 * @throws TransactionNotFoundException if the transaction was not found
	 */
	public Transaction getTransaction(BigInteger transactionNumber) throws TransactionNotFoundException {
		
		if(!transactions.containsKey(transactionNumber))
			throw new TransactionNotFoundException("Transaction with number: " + transactionNumber + " not found");
		
		return transactions.get(transactionNumber);
		
	}
	
	/**
	 * Gets all transactions from EVERY account within a certain time range (INCLUSIVE)
	 * @param startDate - inclusive start date
	 * @param endDate - inclusive end date
	 * @return list of all transactions that occurred within time constraints
	 */
	public List<Transaction> getAllTransactionsRange(LocalDate startDate, LocalDate endDate) {
		List<Transaction> transactionList = new ArrayList<Transaction>();
		long startDay = startDate.toEpochDay();
		long endDay = endDate.toEpochDay();
		
		for(Map.Entry<BigInteger, Transaction> entry : transactions.entrySet()) {
			
			Transaction currTransaction = entry.getValue();
			long transactionDate = currTransaction.getDate().toEpochDay();
			boolean inRange = (transactionDate >= startDay && transactionDate <= endDay);
			
			if(inRange) {
				transactionList.add(currTransaction);
			}
		}
		
		return transactionList;
	}
	
	/**
	 * Gets all transactions from a given account within a certain time range (INCLUSIVE)
	 * @param accountNumber - account number to retrieve transactions from
	 * @param startDate - inclusive start date
	 * @param endDate - inclusive end date
	 * @return list of all transactions from a given account that occurred within time constraints
	 */
	public List<Transaction> getAllTransactionsAccountRange(BigInteger accountNumber, LocalDate startDate, LocalDate endDate) {
		List<Transaction> transactionList = new ArrayList<Transaction>();
		long startDay = startDate.toEpochDay();
		long endDay = endDate.toEpochDay();
		
		for(Map.Entry<BigInteger, Transaction> entry : transactions.entrySet()) {
			
			Transaction currTransaction = entry.getValue();
			long transactionDate = currTransaction.getDate().toEpochDay();
			
			boolean sameAccount = (currTransaction.getAccountNumber().compareTo(accountNumber) == 0);
			boolean inRange = (transactionDate >= startDay && transactionDate <= endDay);
			
			if(sameAccount && inRange) {
				transactionList.add(currTransaction);
			}
			
		}
		
		return transactionList;
	}
	
	/**
	 * Gets all transactions from EVERY account that occurred on a given day
	 * @param date - date to fetch transactions from 
	 * @return list of all transactions that occurred on a given day
	 */
	public List<Transaction> getAllTransactionsDate(LocalDate date) {
		List<Transaction> transactionList = new ArrayList<Transaction>();
		long requestedDate = date.toEpochDay();
		
		for(Map.Entry<BigInteger, Transaction> entry : transactions.entrySet()) {
			
			Transaction currTransaction = entry.getValue();
			long transactionDate = currTransaction.getDate().toEpochDay();
			
			boolean sameDay = (transactionDate == requestedDate);
			
			if(sameDay) {
				transactionList.add(currTransaction);
			}
			
		}
		
		return transactionList;
	}
	
	/**
	 * Gets all transactions from a specified account on a given day
	 * @param accountNumber - account to get transactions from
	 * @param date - date to fetch transactions from
	 * @return list of all transactions from a given account that occurred on a given day
	 */
	public List<Transaction> getAllTransactionsAccountDate(BigInteger accountNumber, LocalDate date) {
		List<Transaction> transactionList = new ArrayList<Transaction>();
		long requestedDate = date.toEpochDay();
		
		for(Map.Entry<BigInteger, Transaction> entry : transactions.entrySet()) {
			
			Transaction currTransaction = entry.getValue();
			long transactionDate = currTransaction.getDate().toEpochDay();
			
			boolean sameAccount = (currTransaction.getAccountNumber().compareTo(accountNumber) == 0);
			boolean sameDay = (transactionDate == requestedDate);
			
			if(sameAccount && sameDay) {
				transactionList.add(currTransaction);
			}
		}
		return transactionList;
	}
	
	/**
	 * Gets all transactions from a specified account that have ever occurred.
	 * @param accountNumber - account to get transactions from
	 * @return list of all transactions that have ever occurred on a specific account
	 */
	public List<Transaction> getAllTransactionsAccount(BigInteger accountNumber) {
		List<Transaction> transactionList = new ArrayList<Transaction>();
		
		for(Map.Entry<BigInteger, Transaction> entry : transactions.entrySet()) {
			Transaction currTransaction = entry.getValue();
			boolean sameAccount = (currTransaction.getAccountNumber().compareTo(accountNumber) == 0);
			
			if(sameAccount) {
				transactionList.add(currTransaction);
			}
		}
		return transactionList;
	}
	

	/**
	 * Saves all transactions to the DAO
	 * @return true if save was successful, false otherwise.
	 */
	public boolean saveTransactions() {
		try {
			return super.saveAll(super.getDB_Name(), transactions);
		} catch(DAOWriteException e) {
			System.out.println("[TRANSACTION DAO] Failed to save all transactions");
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Gets all transactions and populates hashmap
	 * @return true if retrieval was successful, false otherwise.
	 */
	public boolean getAllTransactions() {
		try {
			transactions = super.getAll(super.getDB_Name());
			return true;
		} catch(DAOReadException e) {
			System.out.println("[TRANSACTION DAO] Failed to read all accounts");
			return false;
		}
	}
	
	/**
	 * Gets current instance of DAO (This is a singleton class)
	 * @return the current instance
	 * @throws DAOReadException if a read error occurs when initializing DAO
	 * @throws DAOWriteException if a write error occurs when initializing DAO
	 */
	public static TransactionDAO getInstance() throws DAOReadException, DAOWriteException {
		if(instance == null)
			instance = new TransactionDAO();
		return instance;
	}
	
}
