package us.JohnnyLeek.BankingSystem.Users;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This class serves as the User model, handling data storage for users
 * Global Fields:
 * 		- licenseNumber: The license number of the user 
 * 		- firstName: The users first name
 * 		- middleName: The users middle name
 * 		- lastName: The users last name
 * 		- birthDate: The users birth date
 * 		- occupation: The users occupation
 * 		- address: An address object containing the users address
 * 		- accountNumbers: Account numbers this user is authorized to use
 * @author leek1j
 *
 */
public class User implements Serializable {

	private String firstName;
	private String middleName;
	private String lastName;
	private LocalDate birthDate;
	private String licenseNumber;
	private String occupation;
	private Address address;
	private List<BigInteger> accountNumbers;
	
	public User(String firstName, String middleName, String lastName, LocalDate birthDate,
				String licenseNumber, String occupation, Address address) 
	{
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.licenseNumber = licenseNumber;
		this.occupation = occupation;
		this.address = address;
		accountNumbers = new ArrayList<BigInteger>();
		
	}
	
	
	
	/* 
	 * Getters and Setters
	 */
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public List<BigInteger> getAccountNumbers() {
		return accountNumbers;
	}
	public void setAccountNumbers(List<BigInteger> accountNumbers) {
		this.accountNumbers = accountNumbers;
	}
	
	
	
}
