package us.JohnnyLeek.BankingSystem.Users;

import java.io.Serializable;

/**
 * This class serves as the Address model, handling data storage for addresses
 * Global Fields:
 * 		- licenseNumber: The license number of the user who lives at this address
 * 		- houseNum: The house number of the address
 * 		- apartmentNum: The apartment number of the address
 * 		- street: The street name of the address
 * 		- state: The state of the address
 * 		- zipCode: The zip code of the address
 * @author leek1j
 *
 */
public class Address implements Serializable{

	private String licenseNumber;
	private int houseNum;
	private int apartmentNum;
	private String street;
	private String city;
	private AddressState state;
	private int zipCode;
	
	public Address(String licenseNumber, int houseNum, int apartmentNum, String street, String city, AddressState state, int zipCode) {
		this.licenseNumber = licenseNumber;
		this.houseNum = houseNum;
		this.apartmentNum = apartmentNum;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}

	
	/*
	 * Getters and Setters
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public int getHouseNum() {
		return houseNum;
	}

	public void setHouseNum(int houseNum) {
		this.houseNum = houseNum;
	}

	public int getApartmentNum() {
		return apartmentNum;
	}

	public void setApartmentNum(int apartmentNum) {
		this.apartmentNum = apartmentNum;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity() {
		this.city = city;
	}

	public AddressState getState() {
		return state;
	}

	public void setState(AddressState state) {
		this.state = state;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
}

